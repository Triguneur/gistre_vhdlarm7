library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


entity MUX21_TB is

end entity MUX21_TB;


architecture BENCH of MUX21_TB is

    signal A, B, S : std_logic_vector(3 downto 0);
    signal COM : std_logic;

begin

    process
    begin

    A <= "1111";
    B <= "0000";
    COM <= 'Z';
    wait for 10 ns;

    COM <= '0';
    wait for 10 ns;
    assert S=A report "Error on COM=0" severity warning;

    COM <= '1';
    wait for 10 ns;
    assert S=B report "Error on COM=1" severity warning;

    wait;
    end process;

    MUX : entity work.MUX21 generic map(4)
    port map(
    A => A,
    B => B,
    COM => COM,
    S => S
    );

end architecture;
