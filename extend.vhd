library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


entity EXTEND is
    generic (N : integer := 16);
    port(
    E : in std_logic_vector(N-1 downto 0);
    S : out std_logic_vector(31 downto 0)
    );
end entity;


architecture RTL of EXTEND is
begin


        --data_out(5 downto 0) <= data_in;
        --data_out(15 downto 16) <= (15 downto 6 => data_in(5));
        --slv_16 <= std_logic_vector(resize(signed(slv_8), slv_16'length));

        --S(N-1 downto 0) <= E;
        --S(31 downto 32) <= (31 downto N => E(N-1));

        S <= std_logic_vector(resize(signed(E), 32));


end architecture;
