LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.numeric_std.all;

entity ALU is
    port(
        A, B : in std_logic_vector(31 downto 0);
        OP : in std_logic_vector(1 downto 0);
        Y : out std_logic_vector(31 downto 0);
        N : out std_logic
        );
end ALU;

architecture RTL of ALU is

    signal TMP : std_logic_vector(31 downto 0) := (others => '0');

begin

    TMP <= std_logic_vector(signed(A) + signed(B)) when OP="00" else
           B when OP="01" else
           std_logic_vector(signed(A) - signed(B)) when OP="10" else
           A;

    N <= '1' when (signed(TMP) < 0) else '0';
    Y <= TMP;

end architecture;
