library IEEE;
use IEEE.std_logic_1164.all;
use IEEE.numeric_std.all;


entity INSTRUCTIONS_DECODER is
    port(
        Instruction, PSR : in std_logic_vector(31 downto 0);
        nPCSel, RegWr, AluSrc, PSREn, MemWr, WrSrc, RegSel : out std_logic;
        AluCtr : out std_logic_vector(1 downto 0)
    );
end entity;


architecture RTL of INSTRUCTIONS_DECODER is
    type enum_instructions is (MOV, ADDi, ADDr, CMP, LDR, STR, BAL, BLT);

    signal current_instr : enum_instructions;

begin

    process(Instruction)
    begin

        case Instruction(27 downto 26) is

            when "00" => -- Data Processing
                case Instruction(24 downto 21) is
                    when "1010" => -- CMP
                        current_instr <= CMP;
                    when "1101" => -- MOV
                        current_instr <= MOV;
                    when "0100" => -- ADD
                        case Instruction(25) is
                            when '0' =>
                                current_instr <= ADDr;
                            when '1' =>
                                current_instr <= ADDi;
                            when others => -- Compliant with ghdl
                                current_instr <= ADDr;
                        end case;
                    when others => -- Compliant with ghdl
                        current_instr <= ADDr;
                end case;

            when "01" => -- Data Transfert
                case Instruction(20) is
                    when '0' =>
                        current_instr <= STR;
                    when '1' =>
                        current_instr <= LDR;
                    when others => -- Compliant with ghdl
                        current_instr <= STR;
                end case;

            when "10" => -- Branchements
                case Instruction(30) is
                    when '1' =>
                        current_instr <= BAL;
                    when '0' =>
                        current_instr <= BLT;
                    when others => -- Compliant with ghdl
                        current_instr <= BLT;
                end case;

            when others => -- Compliant with ghdl
                current_instr <= MOV;

        end case;

    end process;

    process(current_instr)
    begin

    -- Set every signal to 0 make the next easier
    nPCSel <= '0';
    RegWr <= '0';
    AluSrc <= '0';
    PSREn <= '0';
    MemWr <= '0';
    WrSrc <= '0';
    RegSel <= '0';
    AluCtr <= "00";

    case current_instr is

        when ADDi =>
            RegWr <= '1';
            AluSrc <= '1';
            PSREn <= '1';
        when ADDr =>
            RegWr <= '1';
            PSREn <= '1';
        when BAL =>
            nPCSel <= '1';
        when BLT =>
            nPCSel <= PSR(0);
        when CMP =>
            AluSrc <= '1';
            AluCtr <= "10";
            PSREn <= '1';
        when LDR =>
            RegWr <= '1';
            AluSrc <= '1';
            PSREn <= '1';
            WrSrc <= '1';
        when MOV =>
            RegWr <= '1';
            AluSrc <= '1';
            AluCtr <= "01";
            PSREn <= '1';
        when STR =>
            AluSrc <= '1';
            PSREn <= '1';
            MemWr <= '1';
            WrSrc <= '1';
            RegSel <= '1';

    end case;

    end process;

end architecture;

