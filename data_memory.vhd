library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


entity DATA_MEMORY is
    port(
    DataOut : out std_logic_vector(31 downto 0);
    DataIn : in std_logic_vector(31 downto 0);
    Addr : in std_logic_vector(5 downto 0);
    WrEn : in std_logic;
    rst, clk : in std_logic
    );
end entity DATA_MEMORY;


architecture RTL of DATA_MEMORY is

type table is array (63 downto 0) of std_logic_vector(31 downto 0);

function init_mem return table is
    variable result : table;
begin
    for i in 63 downto 0 loop
        result(i) := std_logic_vector(to_unsigned(i, 32));
    end loop;
    return result;
end init_mem;

-- Use initialization only when needed
signal RAM : table;
--signal RAM : table := init_mem;

begin

    process(clk, rst)
    begin

    if rst = '1' then
        DataOut <= (others => 'Z');
        for i in 63 downto 0 loop
            RAM(i) <= (others => '0');
        end loop;
        -- Use initialization only when needed
        --RAM <= init_mem;
    end if;
    if rising_edge(clk) then
        if WrEn='1' then
            RAM(to_integer(unsigned(Addr))) <= DataIn;
        end if;
    end if;

    end process;

    DataOut <= RAM(to_integer(unsigned(Addr)));

end architecture;

