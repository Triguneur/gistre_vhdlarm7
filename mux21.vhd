library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


entity MUX21 is
    generic(N : integer := 2);
    port(
    A, B : in std_logic_vector(N-1 downto 0);
    COM : in std_logic;
    S : out std_logic_vector(N-1 downto 0)
    );
end entity MUX21;


architecture RTL of MUX21 is
begin
    process(A, B, COM)
    begin

    if COM = '0' then
        S <= A;
    elsif COM = '1' then
        S <= B;
    else
        S <= (others => 'Z');
    end if;

    end process;

end architecture;
