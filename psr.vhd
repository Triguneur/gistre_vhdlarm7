library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


entity PSR is
    port(
    rst, clk, WE : in std_logic;
    DataOut : out std_logic_vector(31 downto 0);
    DataIn : in std_logic
    );
end entity PSR;


architecture RTL of PSR is

signal STATE : std_logic_vector(31 downto 0) := (others => '0');

begin

    process(clk, rst)
    begin
        if rst='1' then
            STATE <= (others => '0');
        end if;
        if rising_edge(clk) then
            STATE(0) <= DataIn;
        end if;
    end process;

    DataOut <= STATE;

end architecture;

