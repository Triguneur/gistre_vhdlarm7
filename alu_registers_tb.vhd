library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


entity ALU_REGISTERS_TB is

end entity ALU_REGISTERS_TB;

architecture Bench of ALU_REGISTERS_TB is

    signal W : std_logic_vector(31 downto 0);
    signal A, B : std_logic_vector(31 downto 0) := (others => '0');
    signal RA, RB , RW : std_logic_vector(3 downto 0) := (others => '0');
    signal WE, clk, rst: std_logic;
    signal OP : std_logic_vector(1 downto 0);

begin

    rst <= '1', '0' after 1 ns;
    process
    begin

    
        -- Initialization of variables

        WE <= '0';
        clk <= '0';
        wait for 10 ns;

        -- R(1) = R(15)
        RA <= "1111";
        RB <= "0000";
        OP <= "11";
        RW <= "0001";
        WE <= '1';
        wait for 5 ns;
        clk <= '1';
        wait for 5 ns;
        clk <= '0';

        -- R(1) = R(1) + R(15)
        RA <= "1111";
        RB <= "0001";
        OP <= "00";
        RW <= "0001";
        WE <= '1';
        wait for 5 ns;
        clk <= '1';
        wait for 5 ns;
        clk <= '0';

        -- R(2) = R(1) + R(15)
        RA <= "1111";
        RB <= "0001";
        OP <= "00";
        RW <= "0010";
        WE <= '1';
        wait for 5 ns;
        clk <= '1';
        wait for 5 ns;
        clk <= '0';

        -- R(3) = R(1) - R(15)
        RA <= "0001";
        RB <= "1111";
        OP <= "10";
        RW <= "0011";
        WE <= '1';
        wait for 5 ns;
        clk <= '1';
        wait for 5 ns;
        clk <= '0';

        -- R(5) = R(7) - R(15)
        RA <= "0100";
        RB <= "1111";
        OP <= "10";
        RW <= "0101";
        WE <= '1';
        wait for 5 ns;
        clk <= '1';
        wait for 5 ns;
        clk <= '0';

        wait;
    end process;

    ALU_REG : entity work.ALU_REGISTERS port map(
    OP => OP,
    RW => RW,
    RA => RA,
    RB => RB,
    WE => WE,
    clk => clk,
    rst => rst
    );

end architecture;
