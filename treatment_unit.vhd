library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


entity TREATMENT_UNIT is
port (RD, RN, RM : in std_logic_vector(3 downto 0);
      RegWe, WrEn, SEL1, SEL2, RegSel, clk, rst : in std_logic;
      OP : in std_logic_vector(1 downto 0);
      InExt : in std_logic_vector(7 downto 0);
      FLAG : out std_logic);

end entity TREATMENT_UNIT;



architecture RTL of TREATMENT_UNIT is

    signal busW, busA, busB, AluOut, DataOut, ExtOut, Mux1Out : std_logic_vector(31 downto 0);
    signal RB : std_logic_vector(3 downto 0);

begin

    MUXReg : entity work.MUX21 generic map(4)
    port map(
    A => RM,
    B => RD,
    COM => RegSel,
    S => RB
    );

    REG : entity work.BENCH_REGISTERS port map(
    W => busW,
    RA => RN,
    RB => RB,
    RW => RD,
    WE => RegWe,
    A => busA,
    B => busB,
    clk => clk,
    rst => rst
    );

    MUX1 : entity work.MUX21 generic map(32)
    port map(
    A => busB,
    B => ExtOut,
    COM => SEL1,
    S => Mux1Out
    );

    ALU : entity work.ALU port map(
    A => busA,
    B => Mux1Out,
    OP => OP,
    Y => AluOut,
    N => FLAG
    );

    MUX2 : entity work.MUX21 generic map(32)
    port map(
    A => AluOut,
    B => DataOut,
    COM => SEL2,
    S => busW
    );

    DATA : entity work.DATA_MEMORY port map(
    DataOut => DataOut,
    DataIn => busB,
    Addr => AluOut(5 downto 0),
    WrEn => WrEn,
    rst => rst,
    clk => clk
    );

    EXT : entity work.EXTEND generic map(8)
    port map(
    E => InExt,
    S => ExtOut
    ); 

end architecture;

