library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity INSTRUCTIONS_MANAGER_TB is

end entity INSTRUCTIONS_MANAGER_TB;

architecture BENCH of INSTRUCTIONS_MANAGER_TB is

    signal nPCsel, rst, clk : std_logic;
    signal Offset : std_logic_vector(23 downto 0);
    signal Instruction : std_logic_vector(31 downto 0);

begin

    process
    begin

    nPCsel <= '0';
    Offset <= (others => '0');

    rst <= '1';
    wait for 1 ns;
    rst <= '0';

    for i in 8 downto 0 loop
        clk <= '1';
        wait for 10 ns;
        clk <= '0';
        wait for 10 ns;
    end loop;

    wait;
    end process;


    INSTRUCTIONS_MANAGER : entity work.INSTRUCTIONS_MANAGER port map(
    nPCsel => nPCsel,
    rst => rst,
    clk => clk,
    Offset => Offset,
    Instruction => Instruction
    );

end architecture;

