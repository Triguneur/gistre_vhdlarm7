library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity DATA_MEMORY_TB is

end entity DATA_MEMORY_TB;

architecture Bench of DATA_MEMORY_TB is

    signal DataIn, DataOut : std_logic_vector(31 downto 0);
    signal Addr : std_logic_vector(5 downto 0) := "000000";
    signal WrEn, clk, rst: std_logic;

begin

    process

    variable AddrTemp : std_logic_vector(5 downto 0) := "000000";
    variable DataTemp : std_logic_vector(31 downto 0);

    begin

        -- Initialization of variables

        rst <= '1';
        wait for 1 ns;
        rst <= '0';

        clk <= '0';
        wait for 10 ns;

        -- Write Test

        for i in 0 to 2**6-1 loop
            AddrTemp := std_logic_vector(to_unsigned(i, 6));
            DataTemp := (31 downto AddrTemp'length => '0') & AddrTemp;
            Addr <= AddrTemp;
            DataIn <= DataTemp;
            WrEn <= '1';
            wait for 5 ns;
            clk <= '1';
            wait for 5 ns;
            clk <= '0';
        end loop;

        -- Reset variables

        WrEn <= '0';
        Addr <= (others => 'Z');

        -- Read Test for A and B

        for i in 0 to 2**6-1 loop
            AddrTemp := std_logic_vector(to_unsigned(i, 6));
            DataTemp := (31 downto AddrTemp'length => '0') & AddrTemp;
            Addr <= AddrTemp;
            wait for 5 ns;
            clk <= '1';
            assert DataOut=DataTemp report "C'est la merde" severity warning;
            wait for 5 ns;
            clk <= '0';
        end loop;

        report "End of test" severity note;
        wait;
    end process;

    RAM: entity work.DATA_MEMORY port map(
        DataIn => DataIn,
        Addr => Addr,
        WrEn => WrEn,
        DataOut => DataOut,
        clk => clk,
        rst => rst
        );

end architecture;

