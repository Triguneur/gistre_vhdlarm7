library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


entity INSTRUCTIONS_MANAGER is
    port(
    nPCsel, rst, clk : in std_logic;
    Offset : in std_logic_vector(23 downto 0);
    Instruction : out std_logic_vector(31 downto 0)
    );
end entity INSTRUCTIONS_MANAGER;


architecture RTL of INSTRUCTIONS_MANAGER is

    signal PCout, ExtOut, UpPC : std_logic_vector(31 downto 0);

begin

    EXT : entity work.EXTEND generic map(24)
    port map(
    E => Offset,
    S => ExtOut
    );

    UPDATE_PC : entity work.UPDATE_PC port map(
    nPCsel => nPCsel,
    Offset => ExtOut,
    DataIn => PCout,
    DataOut => UpPC
    );

    PC : entity work.REGISTER_PC port map(
    rst => rst,
    clk => clk,
    DataIn => UpPC,
    DataOut => PCout
    );

    INST_MEM : entity work.instruction_memory port map(
    PC => PCout,
    Instruction => Instruction
    );

end architecture;

