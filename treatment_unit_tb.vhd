library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity TREATMENT_UNIT_TB is

end entity TREATMENT_UNIT_TB;

architecture BENCH of TREATMENT_UNIT_TB is

    signal RW, RA, RB : std_logic_vector(3 downto 0);
    signal RegWe, WrEn, SEL1, SEL2, clk, rst, FLAG : std_logic;
    signal OP : std_logic_vector(1 downto 0);
    signal InExt : std_logic_vector(7 downto 0);
    signal RegSel : std_logic := '0';

begin

    process
    begin
    rst <= '1';
    wait for 1 ns;
    rst <= '0';

    -- Initialization of signals

    RW <= (others => 'Z');
    RA <= (others => 'Z');
    RB <= (others => 'Z');
    RegWe <= '0';
    WrEn <= '0';
    SEL1 <= 'Z';
    SEL2 <= 'Z';
    clk <= '0';
    rst <= '0';
    FLAG <= 'Z';
    OP <= "ZZ";
    InExt <= (others => '0');

    -- Addition of two values in register

    RA <= "1111";
    RB <= "0001";
    RW <= "0001";
    OP <= "00";
    RegWe <= '1';
    SEL1 <= '0';
    SEL2 <= '0';
    wait for 5 ns;
    clk <= '1';
    wait for 5 ns;
    clk <= '0';

    -- Addition of 1 value in register with 1 direct value

    InExt <= X"23";
    SEL1 <= '1';
    RW <= "0010";
    wait for 5 ns;
    clk <= '1';
    wait for 5 ns;
    clk <= '0';


    -- Substraction of two registers

    SEL1 <= '0';
    RA <= "0010";
    RB <= "1111";
    RW <= "0011";
    OP <= "10";
    wait for 5 ns;
    clk <= '1';
    wait for 5 ns;
    clk <= '0';

    -- Substraction of 1 value in register by one direct value

    InExt <= X"12";
    SEL1 <= '1';
    RA <= "0011";
    RW <= "0100";
    wait for 5 ns;
    clk <= '1';
    wait for 5 ns;
    clk <= '0';

    -- Copy of a register to an other register

    OP <= "11";
    RW <= "0101";
    RA <= "0100";
    SEL1 <= 'Z';
    wait for 5 ns;
    clk <= '1';
    wait for 5 ns;
    clk <= '0';

    -- Copy of a register to memory

    RA <= "0000";
    RB <= "0101";
    RegWe <= '0';
    WrEn <= '1';
    wait for 5 ns;
    clk <= '1';
    wait for 5 ns;
    clk <= '0';

    -- Copy from memory to register

    RW <= "0110";
    RegWe <= '1';
    WrEn <= '0';
    SEL2 <= '1';
    wait for 5 ns;
    clk <= '1';
    wait for 5 ns;
    clk <= '0';

    wait;
    end process;

    TREATMENT_UNIT : entity work.TREATMENT_UNIT port map(
    RD => RW,
    RN => RA,
    RM => RB,
    RegWe => RegWe,
    WrEn => WrEn,
    RegSel => RegSel,
    SEL1 => SEL1,
    SEL2 => SEL2,
    clk => clk,
    rst => rst,
    OP => OP,
    InExt => InExt,
    FLAG => FLAG
    );

end architecture;

