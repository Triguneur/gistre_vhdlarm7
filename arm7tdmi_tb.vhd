library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity ARM7TDMI_TB is

end entity ARM7TDMI_TB;

architecture BENCH of ARM7TDMI_TB is

    signal clk, rst : std_logic;

begin

    process
    begin
    clk <= '0';
    rst <= '1';
    wait for 10 ns;
    rst <= '0';

    for i in 0 to 10 loop
        clk <= '0';
        wait for 5 ns;
        clk <= '1';
        wait for 5 ns;
    end loop;
    wait;

    end process;

    ARM7TDMI : entity work.ARM7TDMI port map(
    clk => clk,
    rst => rst
    );

end architecture;

