library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


entity EXTEND_TB is

end entity EXTEND_TB;


architecture BENCH of EXTEND_TB is

    signal E : std_logic_vector(7 downto 0);
    signal S : std_logic_vector(31 downto 0);

begin

    process
    begin

        for i in -127 to 127 loop
            E <= std_logic_vector(to_signed(i, 8));
            wait for 5 ns;
            assert signed(S)=signed(E) report "Error on extend sign" severity warning;
        end loop;

        wait;
    end process;

    EXT : entity work.EXTEND generic map(8)
    port map(
    E => E,
    S => S
    );
end architecture;
