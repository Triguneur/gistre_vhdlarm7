library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


entity ALU_REGISTERS is
port (OP : in std_logic_vector(1 downto 0);
      RW, RA, RB : in std_logic_vector(3 downto 0);
      WE, rst, clk : in std_logic);

end entity ALU_REGISTERS;


architecture RTL of ALU_REGISTERS is

    signal A, B, W : std_logic_vector(31 downto 0);

begin

    REG : entity work.BENCH_REGISTERS port map(
    W => W,
    RA => RA,
    RB => RB,
    RW => RW,
    WE => WE,
    A => A,
    B => B,
    clk => clk,
    rst => rst
    );

    ALU : entity work.ALU port map(
    OP => OP,
    A => A,
    B => B,
    Y => W
    );


end architecture;
