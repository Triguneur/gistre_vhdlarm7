library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity BENCH_REGISTERS_TB is

end entity BENCH_REGISTERS_TB;

architecture Bench of BENCH_REGISTERS_TB is

    signal W, A, B : std_logic_vector(31 downto 0);
    signal RA : std_logic_vector(3 downto 0) := "0000";
    signal RB : std_logic_vector(3 downto 0) := "0000";
    signal RW : std_logic_vector(3 downto 0);
    signal WE, clk, rst: std_logic;

begin

    rst <= '1' , '0' after 1 ns;
    process

    variable AddrTemp : std_logic_vector(3 downto 0) := (others => '0');
    variable DataTemp : std_logic_vector(31 downto 0);

    begin

        -- Initialization of variables

        RA <= (others => '0');
        RB <= (others => '0');
        clk <= '0';
        wait for 10 ns;

        -- Write Test

        for i in 0 to 2**4-1 loop
            AddrTemp := std_logic_vector(to_unsigned(i, 4));
            DataTemp := (31 downto AddrTemp'length => '0') & AddrTemp;
            RW <= AddrTemp;
            W <= DataTemp;
            WE <= '1';
            wait for 5 ns;
            clk <= '1';
            wait for 5 ns;
            clk <= '0';
        end loop;

        -- Reset variables

        WE <= '0';
        RW <= (others => 'Z');

        -- Read Test for A and B

        for i in 0 to 2**4-1 loop
            AddrTemp := std_logic_vector(to_unsigned(i, 4));
            DataTemp := (31 downto AddrTemp'length => '0') & AddrTemp;
            RA <= AddrTemp;
            wait for 5 ns;
            clk <= '1';
            assert A=DataTemp report "C'est la merde" severity warning;
            wait for 5 ns;
            clk <= '0';
        end loop;

        for i in 0 to 2**4-1 loop
            AddrTemp := std_logic_vector(to_unsigned(i, 4));
            DataTemp := (31 downto AddrTemp'length => '0') & AddrTemp;
            RB <= AddrTemp;
            wait for 5 ns;
            clk <= '1';
            assert B=DataTemp report "C'est la merde" severity warning;
            wait for 5 ns;
            clk <= '0';
        end loop;

        report "End of test" severity note;
        wait;
    end process;

    RAM: entity work.BENCH_REGISTERS port map(
        W => W,
        RA => RA,
        RB => RB,
        RW => RW,
        WE => WE,
        A => A,
        B => B,
        clk => clk,
        rst => rst
        );

end architecture;

