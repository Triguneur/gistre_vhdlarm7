COMP=ghdl-gcc
GHDLFLAGS=--std=93c

OBJ=instruction_memory.o alu.o alu_tb.o bench_registers.o bench_registers_tb.o alu_registers.o alu_registers_tb.o mux21.o mux21_tb.o extend.o extend_tb.o data_memory.o data_memory_tb.o treatment_unit.o treatment_unit_tb.o register_pc.o update_pc.o instructions_manager.o instructions_manager_tb.o psr.o instructions_decoder.o arm7tdmi.o arm7tdmi_tb.o

# Binary depends on the object file
%: %.o
	$(COMP) -e $(GHDLFLAGS) $@

# Object file depends on source
%.o: %.vhd
	$(COMP) -a $(GHDLFLAGS) $<

alu_tb: $(OBJ)
	$(COMP) -e $@
	$(COMP) -r $@ --vcd=$@.vcd
	gtkwave $@.vcd --rcvar 'do_initial_zoom_fit yes' --rcvar 'enable_vcd_autosave yes'

bench_registers_tb: $(OBJ)
	$(COMP) -e $@
	$(COMP) -r $@ --vcd=$@.vcd
	gtkwave $@.vcd --rcvar 'do_initial_zoom_fit yes' --rcvar 'enable_vcd_autosave yes'

alu_registers_tb: $(OBJ)
	$(COMP) -e $@
	./$@ --wave=tmp.ghw
	gtkwave tmp.ghw

mux21_tb: $(OBJ)
	$(COMP) -e $@
	$(COMP) -r $@ --vcd=$@.vcd
	gtkwave $@.vcd --rcvar 'do_initial_zoom_fit yes' --rcvar 'enable_vcd_autosave yes'

extend_tb: $(OBJ)
	$(COMP) -e $@
	$(COMP) -r $@ --vcd=$@.vcd
	gtkwave $@.vcd --rcvar 'do_initial_zoom_fit yes' --rcvar 'enable_vcd_autosave yes'

data_memory_tb: $(OBJ)
	$(COMP) -e $@
	$(COMP) -r $@ --vcd=$@.vcd
	gtkwave $@.vcd --rcvar 'do_initial_zoom_fit yes' --rcvar 'enable_vcd_autosave yes'

treatment_unit_tb: $(OBJ)
	$(COMP) -e $@
	./$@ --wave=treatment_unit.ghw
	gtkwave treatment_unit.ghw

instructions_manager_tb: $(OBJ)
	$(COMP) -e $@
	./$@ --wave=instructions_manager.ghw
	gtkwave instructions_manager.ghw

arm7tdmi_tb: $(OBJ)
	$(COMP) -e $@
	./$@ --wave=arm7tdmi.ghw
	gtkwave arm7tdmi.ghw

check: $(OBJ)
	$(COMP) -e alu_tb
	$(COMP) -r alu_tb
	$(COMP) -e bench_registers_tb
	$(COMP) -r bench_registers_tb
	$(COMP) -e mux21_tb
	$(COMP) -r mux21_tb
	$(COMP) -e extend_tb
	$(COMP) -r extend_tb
	$(COMP) -e data_memory
	$(COMP) -r data_memory
	@echo "Check if any assert was thrown"


clean:
	$(RM) *.vcd *.sav *.cf *.o *.ghw $(OBJ:.o=)

all: alu_tb
