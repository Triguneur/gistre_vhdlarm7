LIBRARY ieee;
USE ieee.std_logic_1164.all;
USE ieee.numeric_std.all;

entity ALU_TB is

end ALU_TB;

architecture BENCH of ALU_TB is

    signal OP : std_logic_vector(1 downto 0);
    signal A : std_logic_vector(31 downto 0) := (others => '1');
    signal B : std_logic_vector(31 downto 0) := (others => '0');
    signal Y : std_logic_vector(31 downto 0);
    signal N : std_logic;

begin

    process
    begin

        OP <= "00";
        wait for 10 ns;
        assert Y=A report "Error on OP=00" severity warning;

        OP <= "01";
        wait for 10 ns;
        assert Y=B report "Error on OP=01" severity warning;

        OP <= "10";
        wait for 10 ns;
        assert Y=A report "Error on OP=10" severity warning;

        OP <= "11";
        wait for 10 ns;
        assert Y=A report "Error on OP=11" severity warning;

        report "End of tests";
        wait;


    end process;

    UUT : entity work.ALU
    port map(
            OP => OP,
            A => A,
            B => B,
            Y => Y,
            N => N
            );

end architecture;
