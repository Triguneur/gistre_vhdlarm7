library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


entity REGISTER_PC is
    port(
    rst, clk : in std_logic;
    DataIn : in std_logic_vector(31 downto 0);
    DataOut : out std_logic_vector(31 downto 0)
    );
end entity REGISTER_PC;


architecture RTL of REGISTER_PC is

    signal PC : std_logic_vector(31 downto 0) := (others => '0');

    begin

    process (clk, rst)
    begin
        if rst='1' then
            PC <= (others => '0');
        end if;
        if rising_edge(clk) then
            PC <= DataIn;
        end if;
    end process;

    DataOut <= PC;

end architecture;

