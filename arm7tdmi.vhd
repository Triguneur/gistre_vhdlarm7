library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity ARM7TDMI is
port(
    clk, rst : in std_logic
    );
end entity ARM7TDMI;


architecture RTL of ARM7TDMI is

    signal RegWr, nPCSel, RegSel, AluSrc, MemWr, WrSrc, PSREn, Flag, WrEn : std_logic;
    signal busA, busB, busW, ALUOut, DataOut, DataIn, ExtOut, Instruction, Addr, PSROut, MuxBOut : std_logic_vector(31 downto 0);
    signal AluCtr : std_logic_vector(1 downto 0);

begin

    PSR : entity work.PSR port map(
    clk => clk,
    rst => rst,
    WE => PSREn,
    DataIn => Flag,
    DataOut => PSROut
    );

    INSTRUCTIONS_MANAGER : entity work.INSTRUCTIONS_MANAGER port map(
    nPCsel => nPCSel,
    rst => rst,
    clk => clk,
    Instruction => Instruction,
    Offset => Instruction(23 downto 0)
    );

    INSTRUCTIONS_DECODER : entity work.INSTRUCTIONS_DECODER port map(
    PSR => PSROut,
    Instruction => Instruction,
    nPCSel => nPCSel,
    RegWr => RegWr,
    AluSrc => AluSrc,
    PSREn => PSREn,
    MemWr => MemWr,
    RegSel => RegSel,
    AluCtr => AluCtr,
    WrSrc => WrSrc
    );

    TREATMENT_UNIT : entity work.TREATMENT_UNIT port map(
    RD => Instruction(15 downto 12),
    RN => Instruction(19 downto 16),
    RM => Instruction(3 downto 0),
    RegWe => RegWr,
    WrEn => WrEn,
    SEL1 => AluSrc,
    SEL2 => WrSrc,
    RegSel => RegSel,
    clk => clk,
    rst => rst,
    OP => AluCtr,
    InExt => Instruction(7 downto 0),
    FLAG => Flag
    );

end architecture;

