library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;


entity UPDATE_PC is
    port(
    nPCsel : in std_logic;
    Offset, DataIn : in std_logic_vector(31 downto 0);
    DataOut : out std_logic_vector(31 downto 0)
    );
end entity UPDATE_PC;


architecture RTL of UPDATE_PC is

begin

    DataOut <= std_logic_vector(signed(DataIn) + 1) when nPCsel='0'
    else std_logic_vector(signed(DataIn) + signed(Offset) + 1);

end architecture;

